#!/usr/bin/env Rscript


# changes: 
#       cluster distribution postscript file is not generated - it is too big and not useful


library(getopt)
library(methods)
library(igraph)
library(multicore)

opt = getopt(matrix(c(
  'input', 'i', 1, "character",
  'output','o', 1, "character",
  'min.cluster.size', 'm', 2, "integer",
  'layout','l',0,'logical',
  'numberOfReads','n',1,'numeric',
  'help','h',0,'logical' ),ncol=4,byrow=T))

if (!is.null(opt$help)) {
cat("Usage:\n\n -i   input file \n -o   output.file\n -m   minimal cluster size (default 50)\n -l   caculate layout\n -n   total number of reads\n")
stop()
}

## Input data
input.file=opt$input
output.file=opt$output
WP=2
if (is.null(opt$min.cluster.size)) {
        min.cluster.size=50
}else{
        min.cluster.size=as.numeric(opt$min.cluster.size)
}



if (is.null(opt$numberOfReads) | (is.na(as.numeric(opt$numberOfReads)))) {
        cat("you must provide -n [total number of reads] ")
        stop()
}

NumOfReads=as.numeric(opt$numberOfReads)
        

#################################################################
## Functions ####################################################
#################################################################
interconnected=function(G,vs1,vs2){
  nv1=unique(unlist(neighborhood(G,order=1,vs1)))
  nv2=unique(unlist(neighborhood(G,order=1,vs2)))
  c1=length(intersect(nv1,vs2))
  c2=length(intersect(nv2,vs1))
  ne=c(c1,c2)
  ne
}

fastgreedy=function(GG){
        eb=fastgreedy.community(GG)
        cutoff=which(eb$modularity==max(eb$modularity))-1  # select cutoff
        c2mb=community.to.membership(GG,eb$merges,cutoff)
        mb=c2mb$membership
        nc=length(unique(mb))   # number of clusters
        N=vcount(GG) 
        new.mb=mb
        new.mb=as.numeric(factor(new.mb,levels=sort(unique(new.mb))[order(rle(sort(new.mb))$length,decreasing=T)] ))-1
        mb.counts=rle(sort(new.mb))$length
        mb.names=rle(sort(new.mb))$values
        CL.names=paste("CL",mb.names+1,sep="")
        FG=list(mb=new.mb,counts=mb.counts,names=mb.names)
        FG
}

num2col=function(x,color.palette,limit=NULL,...){
        if (is.null(limit)){limit=range(x)}     
        n=length(color.palette)-1
        m=n/(limit[2]-limit[1])
        x[x<limit[1]]=limit[1]
        x[x>limit[2]]=limit[2]
        x=x-limit[1]
        index=round(m*x,digits=0)+1
        col.output=color.palette[index]
        col.output
}
# for alternative faster graph ploting:
plotg=function(GG,LL,wlim=NULL,...){
        
        e=get.edgelist(GG,names=F)+1
        w=E(GG)$weight
        if (!is.null(wlim)) {e=e[w>wlim,]; w=w[w>wlim]}
        X0=LL[e[,1],1]
        Y0=LL[e[,1],2]
        X1=LL[e[,2],1]
        Y1=LL[e[,2],2]
        plot(range(LL[,1]),range(LL[,2]),xlab="",ylab="",axes=F,type="n",...)
        brv=num2col(rank(w),rev(grey.colors(50)))
        segments(X0,Y0,X1,Y1,lwd=.5,col=brv)
        points(LL,pch=18,cex=.4,...)
}

indexing=function(x,ind){
  y=as.numeric(factor(ind,levels=x))
  y
}

graph.data.frame2 <- function(d, directed=TRUE) {

  if (ncol(d) < 2) {
    stop("the data frame should contain at least two columns")
  }

  # assign vertex ids
  if (nrow(d)>200000000){
    cat('getting unique...')
    names <- uniqueM(c(d[,1], d[,2]))
    cat('done\n')
  }else{
    names <- unique( c(as.character(d[,1]), as.character(d[,2])) )
  }

  ids <- seq(along=names)-1
  names(ids) <- names

  # create graph
  g <- graph.empty(n=0, directed=directed)
  g <- add.vertices(g, length(ids), name=names)

  # create edge list
  from <- as.character(d[,1])
  to <- as.character(d[,2])
  #edges <- t(matrix(c(ids[from], ids[to]), nc=2))
  edges <- t(matrix(c(indexing(names,from),indexing(names,to)),nc=2))-1

  # edge attributes
  attrs <- list()
  if (ncol(d) > 2) {
    for (i in 3:ncol(d)) {
      newval <- d[,i]
      if (class(newval) == "factor") {
        newval <- as.character(newval)
      }
      attrs[[ names(d)[i] ]] <- newval
    }
  }
  
  # add the edges
  g <- add.edges(g, edges, attr=attrs)
  g
}


Unique=function(x){
    require(hash)
    h=hash()
    for (i in x){
          h[i]=1
    }
  keys(h)
}

uniqueM=function(x,n=100)
{ print(length(x)  )
  x=unlist(mclapply(split(x,1:4),FUN=unique))
  print(length(x))  
  x=unique(x)
  print(length(x)  )
  x
}


################################################################
##  End of Functions ###########################################
################################################################

cat("loading input file")
hitsort=read.table(input.file, sep="\t",as.is=T,header=F)
cat("...done\n")

cat("creating graph object\n")
colnames(hitsort)=c(1,2,"weight")
hitsort[,3]=hitsort[,3]^WP
G=graph.data.frame2(hitsort,directed=F)
#G=graph.edgelist(as.matrix(hitsort[,1:2]),directed=F)
#E(G)$weight=hitsort[,3]^WP
G=simplify(G)

igraph.par("verbose",T)

cat("input data:\n")
cat(paste("number of edges:",ecount(G)),"\n")
cat(paste("number of reads:",vcount(G)),"\n")
cat("-----------------------------------------------\n")

cat("fastgreedy clustering")
eb=fastgreedy.community(G)


# analyze fastgreedy clustering
cutoff=which(eb$modularity==max(eb$modularity))-1  # select cutoff
c2mb=community.to.membership(G,eb$merges,cutoff)
mb=c2mb$membership
nc=length(unique(mb))   # number of clusters
N=vcount(G)             # number of reads
new.mb=mb                       

# count cluster size and renumber cluster 0 - bigest cluster
# membership to cluster #
new.mb=as.numeric(factor(new.mb,levels=sort(unique(new.mb))[order(rle(sort(new.mb))$length,decreasing=T)] ))-1 
new.nc=length(unique(new.mb))
mb.counts=rle(sort(new.mb))$length   # number of read in cluster
mb.names=rle(sort(new.mb))$values
CL.names=paste("CL",mb.names+1,sep="")  # cluster names


# mb.counts including 'single' copy reads

mb.counts.All=c(mb.counts,rep(1,NumOfReads-sum(mb.counts)))


cat("tclust clustering")
# tclust like clusters - compare with fastgreedy clusters
# to check potential drops of small fastgreedy subclust
tclustG=clusters(G)
tclustG.mb=as.numeric(factor(tclustG$membership,levels=sort(unique(tclustG$membership))[order(rle(sort(tclustG$membership))$length,decreasing=T)] ))-1
tclustG.counts=rle(sort(tclustG.mb))$length
tclustG.names=rle(sort(tclustG.mb))$values
# size distribution in clusters:
brks=10^(0:floor(log10(N)+1))
fgHist=hist(mb.counts,breaks=brks)
tHist=hist(tclustG.counts,breaks=brks)


# output is a table for each tcluster
outfile="TCLUST_vs_FGclust.txt"

sizeint=c("Size",paste(brks[-length(brks)],"-",brks[-1],sep=""))
fgc=c("FGclustering",fgHist$counts)
tc=c("Tclustering",tHist$counts)

cat("#### Tclust versust fastgreedy clustering #############\n",file=outfile)
cat("cluster size distribution - number of reads within the clusters:\n",file=outfile,append=T)
write.table(format(rbind(sizeint,"--------------",fgc,tc,"--------------")),file=outfile,append=T,col.names=F,row.names=F,quote=F)


cat("writing output file")
## OUTPUT:
#####################################################
##export TCLUST like files with list of IDs in clusters
filecon=file(output.file,open="w")
allIds=V(G)$name
for (i in mb.names){
  Ids=allIds[(new.mb==i)]
  writeLines(paste(">CL",i+1,"\t",length(Ids),sep=""),con=filecon,sep="\n")
  writeLines(paste(Ids,collapse="\t"),con=filecon,sep="\n")
 }
close(filecon)
cat("...done\n")




# export clusters as graph, create subG list containing first NCcluster
cat("exporting clusters in edgelist format\n")
## subgraph output in table format
NC=sum(mb.counts>=min.cluster.size)
subdir="tab/"
dir.create(paste(input.file,"_",subdir,sep=""))
subG=list()  # list of all clusterss as subgraphs for analysis
cat("saving",NC," cluster subgraphs]\n")
for (i in 0:(NC-1)){
  subG[[i+1]]=subgraph(G,(which(new.mb==i)-1))
  subGnames=V(subG[[i+1]])$name
  subG.size=length(subGnames)
  subGmb=(hitsort[,1] %in% subGnames) & (hitsort[,2] %in% subGnames)
  subG.sif=hitsort[subGmb,]
  graph.name=paste(input.file,"_",subdir,i+1,"_",subG.size,".tab",sep="")
  write.table(subG.sif,file=graph.name,sep="\t",quote=F,row.names=F,col.names=F)
  #cat("\b\b\b\b\b")
  cat(i+1)
  }
cat("\n...done\n")
save.image("tmp1.RData")
## calculate for each cluster number of neighbors from surrounding -
## output g graph - vertices are CLuster, edges weights - number of cennections
# G ---->g (original vertices are renamed to CL number, number of edges ~ weight
# collapse vertices from clusters
MB=new.mb
names(MB)=V(G)$name
g=graph.empty(n=0,directed=F) 
g=add.vertices(g,length(CL.names), attr=list(name=CL.names))

names(mb.counts)=CL.names

#V1V2=cbind(paste("CL",unname(MB[hitsort[,1]])+1,sep=""),paste("CL",unname(MB[hitsort[,2]])+1,sep=""))
        # this was too slow for large graphs
  # instead:
V(G)$name=paste('CL',MB+1,sep='')
V1V2=get.edgelist(G)
V1V2=V1V2[V1V2[,1]!=V1V2[,2],]
v1_v2=apply(V1V2,1,paste,collapse="_")

# convert multiple edges to weight weight
ss=order(v1_v2)
v1_v2=v1_v2[ss]
V1V2=V1V2[ss,]
gdf=data.frame(unique(V1V2),rle(v1_v2)$lengths,stringsAsFactors=F)
colnames(gdf)=c(1,2,"weight")
g=graph.data.frame(gdf,directed=F)
V(g)$size=mb.counts[V(g)$name]

# isolated cluster
singletons=CL.names[!(CL.names %in% V(g)$name)]
tclust=clusters(g)
tclustmembership=rep("singletons",length(CL.names))  # for table -CL.names vs membership including singletons
names(tclustmembership)=CL.names

## export analysis relationship between clusters - export connected subgraph where weight
## correspond to number of connected sequences, size number of seqs in cluster
## export of graphs in gml format
subdir="connected_clusters"
dir.create(subdir)

cat("# cluster grouping :\n",file=paste(subdir,"/cluster_grouping.txt",sep=""))

for (i in 0:(tclust$no-1)){
        g.sub=subgraph(g,v=which(tclust$membership==i)-1)
        if (vcount(g.sub)>1){
                write.graph(g.sub,file=paste(subdir,"/TCL",i+1,".gml",sep=""),format="gml")
                cat(paste(">TCL",i+1,"\n",sep=""),file=paste(subdir,"/cluster_grouping.txt",sep=""),append=T)
                cat(paste(V(g.sub)$name[order(as.numeric(gsub("CL","",V(g.sub)$name)))],collapse=" "),file=paste(subdir,"/cluster_grouping.txt",sep=""),append=T,sep="\n")
                tclustmembership[V(g.sub)$name]=paste("TCL",i+1,sep="")
                
        }
}
cat(paste(">singletons","\n",sep=""),file=paste(subdir,"/cluster_grouping.txt",sep=""),append=T)
cat(paste(singletons,collapse=" "),file=paste(subdir,"/cluster_grouping.txt",sep=""),append=T,sep="\n")
write.table(tclustmembership,col.names=F,quote=F,file=paste(subdir,"/cluster_grouping2.txt",sep=""),sep="\t")


cat("done\n")

cat("analysis of subclusters properties...")
## calculate parameters of clusters  
# 1 number of edges and vertices , degree
NV=sapply(subG,vcount)   
NE=sapply(subG,FUN=ecount)
Dgr=sapply(subG,FUN=degree.distribution)  #list
maxDgr=sapply(Dgr,length)
meanDensity=NE/(((NV^2)-NV)/2)
# 2 criteria of linearity and length
spath.hist=mclapply(subG,FUN=function(x)path.length.hist(x,directed=F)$res)   #list
lin1=sapply(spath.hist,FUN=function(x)cor(x,seq_along(x)))
lin1[is.na(lin1)]=0
lin2=sapply(spath.hist,FUN=function(x)-log10(as.numeric(try(cor.test(x,seq_along(x))$p.value,silent=T))))  
lin2[is.na(lin2)]=0
diam=sapply(spath.hist,length)
averpath=unlist(mclapply(subG,FUN=function(x)average.path.length(x,directed=F)))
# 3 criteria based on modularity and fastgreedy clustering
mdl=unlist(mclapply(subG,FUN=function(x)max(fastgreedy.community(x)$modularity)))
wts=unlist(mclapply(subG,FUN=function(x)mean((E(x)$weight)^(1/WP))))
## export parametrs of cluster:
allnames=paste("CL",mb.names[1:NC]+1,sep="")

outputsummary=data.frame(allnames,NV,NE,maxDgr,diam,meanDensity,lin2,mdl,wts)
colnames(outputsummary)=c("ClusterName","NumberOfReads","NumberOfPairs","MaximalDegree","Diameter","meanDensity","linearity","modularity","meanBlastScore")
write.table(outputsummary,file="clustersSummary.cvs",sep=",",col.names=T,row.names=F,quote=F)
cat("done\n")


## calculate all layouts: - this can be omited

if (!is.null(opt$layout)){
        cat("subgraphs layout computation...")
        LL=mclapply(subG,layout.fruchterman.reingold,dim=3,verbose=F) # can take significant time
        cat("done\n")
        
        cat(" saving cluster graph structure..")
        dir.create("GL")
        ## save subgraphs with layouts:
        for (i in seq_along(subG)){     
                GL=list(G=subG[[i]],L=LL[[i]])
                save(GL,file=paste("GL/CL",i,".GL",sep=""))
        }
        postscript("clusters_graph_structure.ps")
        par(mfrow=c(4,4),mar=c(0,0,2,0))
        for (i in seq_along(subG)){
                plotg(subG[[i]],LL[[i]],main=paste("CL",i,sep=""))              
        }
        dev.off()
}
cat("done\n")


## SAVE ALL DATA
cat("saving workspace")
save.image(paste(output.file,".RData",sep=""))
cat("...done\n")


