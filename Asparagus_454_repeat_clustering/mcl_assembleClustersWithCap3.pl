#!/usr/bin/perl
# parse MCL one-cluster-per-line format and assemble MCL clusters using cap3 
# usage : perl mcl_assembleClustersWithCap3.pl <mclClusters> <fastaFile>
# this is slow, a stupid script, but works as intended. someday, someday i'll rewrite this concisely
# harkess


use strict;
use warnings;
use Bio::SeqIO;

my $mcl    = $ARGV[0];
my $fasta  = $ARGV[1];
my %seqs;
my $max    = 15;   # maximum number of clusters to report and assemble

# load up the fasta file into the %seqs hash

my $io_obj = Bio::SeqIO->new (-file => $fasta,
			      -format => 'fasta');
while(my $seq_obj = $io_obj -> next_seq) {
   my $id = $seq_obj -> id();
   my  $seq = $seq_obj -> seq();
   $seqs{$id}=$seq;
}

# iterate through each line, pull out every fasta id and match it to the hash sequence
# stop at $max clusters. push fasta names into array for use in cap3 later
# figure out some sequence statistics while we're at it (% reads of each species for each cluster)

my $count = 0;
open MCL, "<$mcl";

while (<MCL>) {
    $count++;
    if ($count > $max) {next;} 
    else {
	my @seqs = split(/\s+/);
	# get some sequencestatistics for each array - (array in scalar context returns value)
	
	my @officinalis = grep(/officinalis/, @seqs);
	my @aphylus = grep(/aphylus/, @seqs);
	my @stipularis = grep(/stipularis/, @seqs);
	my @densiflorus = grep(/densiflorus/, @seqs);
	my @falcatus = grep(/falcatus/, @seqs);
	my @asparagoidies = grep(/asparagoidies/, @seqs);
	my @moritimus = grep(/moritimus/, @seqs);
	my @pyrimidalis = grep(/pyrimidalis/, @seqs);
	my @virgatus = grep(/virgatus/, @seqs);
        
        # put arrays into scalar context
        my $offcount = @officinalis;
        my $aphcount = @aphylus;
        my $sticount = @stipularis;
        my $dencount = @densiflorus;
        my $falcount = @falcatus;
        my $aspcount = @asparagoidies;
        my $morcount = @moritimus;
        my $pyrcount = @pyrimidalis;
        my $vircount = @virgatus;

        open STAT, ">>", "Clusters.stats";
        print STAT "Cluster$count\n\n";
	print STAT "Dioecious\n==============\n";
	print STAT "officinalis : $offcount\n";
	print STAT "aphylus     : $aphcount\n";
	print STAT "stipularis  : $sticount\n";
	print STAT "maritimus   : $morcount\n";
	print STAT "Hermaphrodite\n=============\n";
	print STAT "densiflorus : $dencount\n";
	print STAT "falcatus    : $falcount\n";
	print STAT "pyrimidalis : $pyrcount\n";
	print STAT "asparagoidies $aspcount\n";
	print STAT "virgatus    : $vircount\n\n\n";

	open CLS, ">Cluster$count.fasta";
	foreach my $curseq (@seqs) {
	    print CLS ">$curseq\n$seqs{$curseq}\n";

	}
    close CLS;
    }
}

close MCL;

# assemble each multifasta with cap3 (defaults)

my @fastas = <Cluster*>;
foreach my $fas (@fastas) {
    print "Starting assembly of $fas\n";
    system "/usr/local/cap3/latest/cap3 $fas";
}




    
    
 
