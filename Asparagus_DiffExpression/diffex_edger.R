setwd('/Users/alexh/Asparagus/aoff_de_rsem')
library(edgeR)
library(limma)

##############
## Goal : Identify genes that are DE between XX, XY, and YY genotypes for 4 
##        Asparagus officinalis breeding lines

x <- read.delim("aoff_RSEM.genecounts", stringsAsFactors=FALSE, check.names=FALSE, row.names=1)
x <- round(x, digits = 0)

group <- factor(c("F","M","supM","F","M","supM","F","M","supM","F","M"))
y <-DGEList(counts=x, group=group)

## filter to keep rows with > 2 counts per million for at least three libraries
keep <- rowSums(cpm(y) > 2) >= 3
y <- y[keep,]
dim(y)

# recalculate library sizes and normalization factors
y$samples$lib.size <- colSums(y$counts)
y <- calcNormFactors(y)

# get normalized count table
normCounts <- cpm(y, normalized.lib.sizes=TRUE)
write.table(normCounts, file="TMMnormCounts.filtered")
#### NESTED INTERACTION FORMULA ####

targets <- readTargets()
BreedingLine <- factor(targets$breedingline)
Gender <- factor(targets$sexgenotype)
design <- model.matrix(~BreedingLine + Gender)

y <- estimateGLMCommonDisp(y,design, verbose=TRUE)
y <- estimateGLMTrendedDisp(y,design)
y <- estimateGLMTagwiseDisp(y,design)
fit <- glmFit(y,design)

par(mfrow=c(2,2))
plotMDS(y, top=500, main="BCV Distance", xlim=c(-2,2))

### Male vs Female
lrt.MvsF <- glmLRT(fit, coef=5)
FDR.MvsF <- p.adjust(lrt.MvsF$table$PValue, method="BH")
sum(FDR.MvsF<0.05)
summary(dt<-decideTestsDGE(lrt.MvsF))
isDE<-as.logical(dt)
DEnamesA=rownames(y)[isDE]
#upregA <-(dt==-1)[isDE]
write(DEnamesA, file="MvsF_RSEM_DE")
plotSmear(lrt.MvsF, de.tags=DEnamesA, main="Male vs Female", smooth.scatter=TRUE)
abline(h=c(-1,1),col="blue")

### Supermale vs Female
lrt.supMvsF <- glmLRT(fit, coef=6)
FDR.supMvsF <- p.adjust(lrt.supMvsF$table$PValue, method="BH")
sum(FDR.supMvsF<0.05)
summary(dt<-decideTestsDGE(lrt.supMvsF))
isDE<-as.logical(dt)
DEnamesB=rownames(y)[isDE]
write(DEnamesB, file="supMvsF_RSEM_DE")
plotSmear(lrt.supMvsF, de.tags=DEnamesB, main="Supermale vs Female", smooth.scatter=TRUE)
abline(h=c(-1,1),col="blue")

### Supermale vs Male
lrt.supMvsM <- glmLRT(fit, contrast=c(0,0,0,0,-1,1))
FDR.supMvsM <- p.adjust(lrt.supMvsM$table$PValue, method="BH")
sum(FDR.supMvsM<0.05)
summary(dt<-decideTestsDGE(lrt.supMvsM))
isDE<-as.logical(dt)
DEnamesC=rownames(y)[isDE]
write(DEnamesC, file="supMvsM_RSEM_DE")
plotSmear(lrt.supMvsM, de.tags=DEnamesC, main="Supermale vs Male", smooth.scatter=TRUE)
abline(h=c(-1,1),col="blue")

## Dosage effects
MvsF.normCounts <- normCounts[DEnamesA,]
MvsF.normCounts.103 <- MvsF.normCounts[,1:3]
MvsF.normCounts.88 <- MvsF.normCounts[,4:6]
MvsF.normCounts.89 <- MvsF.normCounts[,7:9]
MvsF.normCounts.9 <- MvsF.normCounts[,10:11]

supMvsF.normCounts <- normCounts[DEnamesB,]
supMvsF.normCounts.103 <- supMvsF.normCounts[,1:3]
supMvsF.normCounts.88 <- supMvsF.normCounts[,4:6]
supMvsF.normCounts.89 <- supMvsF.normCounts[,7:9]
supMvsF.normCounts.9 <- supMvsF.normCounts[,10:11]

k1.103 <- kmeans(MvsF.normCounts.103,3)
k2.88 <- kmeans(MvsF.normCounts.88,3)
k3.89 <- kmeans(MvsF.normCounts.89,3)
k4.9 <- kmeans(MvsF.normCounts.9,3)

par(mfrow=c(2,2))
parcoord(MvsF.normCounts.103, col=k1.103$cluster)
parcoord(MvsF.normCounts.88, col=k2.88$cluster)
parcoord(MvsF.normCounts.89, col=k3.89$cluster)
parcoord(MvsF.normCounts.9, col=k4.9$cluster)


###### VENN DIAGRAMS

venn.plot <- draw.triple.venn(
area1 = 353,
area2 = 552,
area3 = 66,
n12 = 223,
n23 = 46,
n13 = 16,
n123 = 5,
category = c("Male vs Female", "Supermale vs Female", "Supermale vs Male"),
fill = c("red", "blue", "green"),
euler.d=F,
scaled=F)
pdf("venndiag.pdf")
grid.draw(venn.plot)
dev.off()