#!/bin/bash
perl /usr/local/trinity/latest/util/alignReads.pl --left $1 --right $2 --seqType fq --target $3 --aligner bowtie --output $4 --sort_buffer_size 6G -- -p 4

