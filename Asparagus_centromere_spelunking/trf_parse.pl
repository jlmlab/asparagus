#!/usr/bin/perl
#use strict;
#use warnings;

my $trf_in = "A_officinalis_trf.out";
my $thresh = 100;
open IN, '<', $trf_in;
while (<IN>) {
	chomp;
	my ($start, $stop, $plen, undef, undef, undef, undef, undef, undef, undef, 
		undef, undef, undef, $consensus_seq, $repeat_seq) = split /\s+/;
	if ($plen > $thresh) {
		print ">$start\n$consensus_seq\n";
	}
}
close IN;
