#!/usr/bin/perl

#usage : filter_short_scaffs.pl fastafile lengthcutoff > outfile.fa
use strict;
use warnings;
use Bio::SeqIO;

my $infile = $ARGV[0];
my $cutoff = $ARGV[1];


my $io_obj = Bio::SeqIO->new(-file => $infile, -format => "fasta" );
while (my $seq_obj = $io_obj->next_seq) {
    my $id = $seq_obj->id();
    my $seq = $seq_obj->seq();
    my $len = $seq_obj->length();
	
    if ($len => $cutoff) {
        print ">$id\n$seq\n";
    }
}
