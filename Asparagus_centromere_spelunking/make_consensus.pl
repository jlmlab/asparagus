#!/usr/bin/perl
# usage : perl make_consensus.pl muscle_alignment.fa > muscle_alignment.cons.fa
use strict;
use warnings;
use Bio::AlignIO;

my $infile = $ARGV[0];
my $threshold_percentage = 50;

my $inseq = Bio::AlignIO -> new(-file => $infile, -format => 'fasta');
my $aln = $inseq -> next_aln();
print ">$infile\.consensus\n";
print $aln -> consensus_string($threshold_percentage);


