#!/usr/bin/perl
#use strict;
use warnings;
use Bio::SeqIO;

my @names;

my $in_blast = "candidate_cents.blastn.tophit";
open IN, '<', $in_blast;
while (<IN>) {
	@fields = split /\s+/;
	#print $fields[1];
	push (@names, $fields[1]);
}


my $io_obj = Bio::SeqIO->new(-file => "A_officinalis_WGS_in.454.fasta", -format => "fasta" );
while (my $seq_obj = $io_obj->next_seq) {
	my $id = $seq_obj->id();
	my $seq = $seq_obj->seq();
	
	if ($id ~~ @names) {
		print ">$id\n$seq\n";
	}
}
close IN;