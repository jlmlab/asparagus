#!/usr/bin/perl -w
 
 my $file = $ARGV[0];
 my $size = $ARGV[1];
 #Number of seqs in each split file.
my $type = $ARGV[2];
#Blast program.
my $database = $ARGV[3];
#path to database
 
 my $seqid;
 my $seq;
 
 my $seq_counter=0;
 my $split_count=1;
 
 open IN, "< $file";
 while(<IN>){
 	chomp;
 	if(/>/){
 		$seq_counter++;
 		if($seqid){
 			mkdir "split.".$split_count;
 			chdir ("split.$split_count");
 			open OUT, ">> split.$split_count.fasta";
 			print OUT "$seqid\n$seq\n";
 			close OUT;
 			if($seq_counter == $size){
 				open OUT, "> r$split_count.blastsubmit.sh";
 				print OUT "#!/bin/bash\ncd /iob_home/jlmlab/aharkess/Asparagus_genome/split.$split_count\ntime /usr/local/ncbiblast/latest/bin/blastall -p $type -d $database -i split.$split_count.fasta -o split.$split_count.10g.$type -m 8 -e 1e-10";
 				close OUT;
 				system "chmod +x r$split_count.blastsubmit.sh";
 				system "qsub -q rcc-30d r$split_count.blastsubmit.sh";
 				$seq_counter = 0;
 				$split_count++;
 				
 			}
 			chdir ("../");
 		}

 		$seq=();
 		$seqid = $_;
 	}
 	else{
 		$seq .= $_;
 	}
 }
 close IN;
chdir ("split.$split_count");
open OUT, "> r$split_count.blastsubmit.sh";
print OUT "#!/bin/bash\ncd /iob_home/jlmlab/aharkess/Asparagus_genome/split.$split_count\ntime /usr/local/ncbiblast/latest/bin/blastall -p $type -d $database -i split.$split_count.fasta -o split.$split_count.10g.$type -m 8 -e 1e-10";
close OUT;
system "chmod +x r$split_count.blastsubmit.sh";
system "qsub -q rcc-30d r$split_count.blastsubmit.sh";

